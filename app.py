import twitter
from time import sleep

consumer_key = ''
consumer_secret = ''
access_token_key = ''
access_token_secret = ''

api = twitter.Api(consumer_key, consumer_secret, access_token_key, access_token_secret)

items_list = []

# Get 199 statuses
statuses = api.GetUserTimeline(count=199)
items_list.append(statuses)

# Get 199 retweets
# retweets = api.GetUserRetweets(count=199)
# items_list.append(retweets)

# Get 199 replies
# replies = api.GetReplies(count=199)
# items_list.append(replies)


for items in items_list:
    for item in items:
        print(f"Removing item...")
        try:
            api.DestroyStatus(item.id)
            print('Status has been removed')
        except Exception:
            print('Failsed, skipping...')
        sleep(0.3)